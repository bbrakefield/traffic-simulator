#define _CRT_SECURE_NO_DEPRECATE // Shut up MS, about strcpy(), etc.
#define _CRT_SECURE_NO_WARNINGS  //   just don't say anything.
#include "Road.h"
#include "Intersection.h"
#include "RoadMap.h"
#include <iostream>
#include <string>

//-----------------------------------------
// Default Constructor
//-----------------------------------------
RoadMap::RoadMap()
{
}

//-----------------------------------------
// Default Destructor
//-----------------------------------------
RoadMap::~RoadMap()
{
}

//-----------------------------------------
// Parameterized Constructor
//-----------------------------------------
RoadMap::RoadMap(TrafficSimDataParser *dp)
{
    // Create a collection of roads
   int size = dp->getRoadCount();
   m_roads.reserve(size);
   
   for(int i = 0; i < size; ++i)
   {
        m_roads.push_back(new Road());
   }

   for(vector<Road*>::iterator it = m_roads.begin(); it != m_roads.end(); ++it)
   {
        // temporary variables for copying
        char* temp_name = NULL;
        strcpy(temp_name, (*it)->getRoadName());
        double temp_SPX = (*it)->getSPX();
        double temp_SPY = (*it)->getSPY();
        double temp_EPX = (*it)->getEPX();
        double temp_EPY = (*it)->getEPY();
        int temp_StartIntersection = (*it)->getStartIntersection();
        int temp_EndIntersection = (*it)->getEndIntersection();
        double temp_SpeedLimit = (*it)->getSpeedLimit();
        int temp_NumLanes = (*it)->getNumLanes();

        // store data in temp variables
        dp->getRoadData(temp_name, &temp_SPX, &temp_SPY, &temp_EPX, &temp_EPY, 
                &temp_StartIntersection, &temp_StartIntersection, &temp_SpeedLimit, 
                &temp_NumLanes);

        // set fields to value stored in temp variables
        (*it)->setRoadName(temp_name);
        (*it)->setSPX(temp_SPX);
        (*it)->setSPY(temp_SPY);
        (*it)->setEPX(temp_EPX);
        (*it)->setEPY(temp_EPY);
        (*it)->setStartIntersection(temp_StartIntersection);
        (*it)->setEndIntersection(temp_EndIntersection);
        (*it)->setSpeedLimit(temp_SpeedLimit);
   }

   // Create a collection of intersections
   int size = dp->getIntersectionCount();
   m_intersections.reserve(size);
   
   for(int i = 0; i < size; ++i)
   {
        m_intersections.push_back(new Intersection());
   }

   for(vector<Intersection*>::iterator it = m_intersections.begin(); it != m_intersections.end(); ++it)
   {
        int temp_interID = (*it)->getIDnum();
        double temp_intxpos = (*it)->getCPX();
        double temp_intypos = (*it)->getCPY();
        char* temp_north = NULL;
        char* temp_south = NULL;
        char* temp_east = NULL;
        char* temp_west = NULL;

        strcpy(temp_north, (*it)->getNorthName());
        strcpy(temp_south, (*it)->getSouthName());
        strcpy(temp_east, (*it)->getEastName());
        strcpy(temp_west, (*it)->getWestName());

        // store data in temp variables
        dp->getIntersectionData(&temp_interID, &temp_intxpos, 
            &temp_intypos, temp_north, temp_south, temp_east, 
            temp_west);

         // set fields to value stored in temp variables
        (*it)->setIDnum(temp_interID);
        (*it)->setCPX(temp_intxpos);
        (*it)->setCPY(temp_intypos);
        (*it)->setNorthName(temp_north);
        (*it)->setSouthName(temp_south);
        (*it)->setEastName(temp_east);
        (*it)->setWestName(temp_west);

   }
}
   

//-----------------------------------------
// Get road given a point on it.
//-----------------------------------------
Road* RoadMap::getRoad(double vx, double vy, double dir)
{
    for(vector<Road*>::iterator it = m_roads.begin(); it != m_roads.end(); ++it)
    {
        double ulx = 0.0;
        double uly = 0.0;
        double lrx = 0.0;
        double lry = 0.0;

        double sx = (*it)->getSPX();
        double sy = (*it)->getSPY();
        double ex = (*it)->getEPX();
        double ey = (*it)->getEPY();
        int lanes = (*it)->getNumLanes();
        bool isNSRoad = false; // false/ew by default.

       // Determine road direction.
       if (sx == ex)
       {
            isNSRoad = true;
       }

       // Calculate ULX, ULY, LRX, LRY

       // If this is an NS road.
       if(isNSRoad) 
       {
           ulx = sx - (lanes / 2 * 3.6);
           uly = sy;
           lrx = ex + (lanes / 2 * 3.6);
           lry = ey;
       }
       else // This is an EW road
       {
           ulx = sx;
           uly = sy - (lanes / 2 * 3.6);
           lrx = ex;
           lry = ey + (lanes / 2 * 3.6);
       }

       // set corner member variables so isPointOnRoad can access them
       (*it)->setULX(ulx);
       (*it)->setULY(uly);
       (*it)->setLRX(lrx);
       (*it)->setLRY(lry);

       // is point on the road
       if((*it)->isPointOnRoad)
       {
           if(isNSRoad && ((dir == 90.0) || (dir == 270.0)))
           {
               // point is on this road so return it.
               return *it;
           }
           else if(!isNSRoad && ((dir == 0.0) || (dir == 180.0)))
           {
               // point is on this road so return it.
               return *it;
           }
       }
    }
}

//-----------------------------------------
// Get a pointer to a road given the road name 
//-----------------------------------------
Road* RoadMap::getRoad(char *rdID)
{
	for(vector<Road*>::iterator it = m_roads.begin(); it != m_roads.end(); ++it)
	{
		// Compare each roads name to the argument passed in
		char* temprdID = (*it)->getRoadName();
		if(strcmp(temprdID, rdID) == 0)
		{
			return *it;
		}
		else
		{
			return NULL;
		}
	}
}

//-----------------------------------------
// Get a pointer to an intersection given the ID
//-----------------------------------------
Intersection* RoadMap::getIntersection(int id)
{
    for(vector<Intersection*>::iterator it = m_intersections.begin(); it != m_intersections.end(); ++it)
    {
	    int tempinID = (*it)->getIDnum();
        if(tempinID == id)
        {
            return *it;
        }
        else
        {
            return NULL;
        }
    }
}

//-----------------------------------------
// Get intersection given a point in it
//-----------------------------------------
Intersection* RoadMap::getIntersection(double x, double y)
{
    for(vector<Intersection*>::iterator it = m_intersections.begin(); it != m_intersections.end(); ++it)
    {
        // Init temp variables
        double ulx = 0.0;
        double uly = 0.0;
        double lrx = 0.0;
        double lry = 0.0;
        int numLanesNS = 0;
        int numLanesEW = 0;
        Road * tempNS = NULL;
        Road * tempEW = NULL;
        char * northName = NULL;
        char * southName = NULL;
        char * eastName = NULL;
        char * westName = NULL;

        // fill field with names of roads touch the intersection
        strcpy(northName, (*it)->getNorthName);
        strcpy(southName, (*it)->getSouthName);
        strcpy(eastName, (*it)->getEastName);
        strcpy(westName, (*it)->getWestName);

        // find NS road
        if(northName != NULL)
        {
            tempNS = getRoad(northName);
        }
        else if (southName != NULL)
        {
            tempNS = getRoad(southName);
        }
        else
        {
            cout << "Both roads are NULL. There is no NSRoad. Something is wrong." << endl;
        }
        
        // FindEW road
        if(eastName != NULL)
        {
            tempEW = getRoad(eastName);
        }
        else if (westName != NULL)
        {
            tempEW = getRoad(westName);
        }
        else
        {
            cout << "Both roads are NULL. There is no EWRoad. Something is wrong." << endl;
        }

        // initialize number of lanes for the two roads
        numLanesNS = tempNS->getNumLanes();
        numLanesEW = tempEW->getNumLanes();

        // calculate corner values
        ulx = (*it)->getCPX() - (numLanesNS / 2 * 3.6);
        uly = (*it)->getCPY() - (numLanesEW / 2 * 3.6);
        lrx = (*it)->getCPX() + (numLanesNS / 2 * 3.6);
        lry = (*it)->getCPY() + (numLanesEW / 2 * 3.6);

        // Set member fields to the calculated values
       (*it)->setULX(ulx);
       (*it)->setULY(uly);
       (*it)->setLRX(lrx);
       (*it)->setLRY(lry);

        if((*it)->isPointInIntersection)
        {
            return *it;
        }
        else 
        {
            return NULL;
        }
    }
}

//-----------------------------------------
// Get the intersection the vehicle is currently traveling toward.
//-----------------------------------------
Intersection* RoadMap::getNextIntersection(double vx, double vy, double dir)
{
    Road * curRoad = getRoad(vx, vy, dir);
    Road * westRoad = NULL;
    Road * southRoad = NULL;
    Road * eastRoad = NULL;
    Road * northRoad = NULL;
    
    Intersection * nextIntersec = NULL;
    double minDist = 655536.999;

    for(vector<Intersection*>::iterator it = m_intersections.begin(); it != m_intersections.end(); ++it)
    {
        northRoad = getRoad((*it)->getNorthName());
        southRoad = getRoad((*it)->getSouthName());
        eastRoad = getRoad((*it)->getEastName());
        westRoad = getRoad((*it)->getWestName());
        bool checkIntersection = false;

        // Is this intersection directely ahead of the vehicle on the current road?
        if((dir == 0.0) && (westRoad == curRoad) && (vx < (*it)->getCPX()))
        {
            checkIntersection = true;
        }
        else if((dir == 90.0) && (southRoad == curRoad) && (vy > (*it)->getCPY()))
        {
            checkIntersection = true;
        }
        else if((dir == 180.0) && (eastRoad == curRoad) && (vx > (*it)->getCPX()))
        {
            checkIntersection = true;
        }
        else if((dir == 270.0) && (northRoad == curRoad) && (vy < (*it)->getCPY()))
        {
            checkIntersection = true;
        }

        if(checkIntersection)
        {
            double dist = sqrt(pow(vx-((*it)->getCPX), 2.0) + pow(vy - ((*it)->getCPY()), 2.0));
            if(dist < minDist)
            {
                minDist = dist;
                nextIntersec = *it;
            }
        }
    }
    return nextIntersec;
}

//-----------------------------------------
// Call each Intersection update()
//-----------------------------------------
void RoadMap::update(double time)
{
	// call each intersection.update
	for(vector<Intersection*>::iterator it = m_intersections.begin(); it != m_intersections.end(); ++it)
	{
		// may need to pass a double parameter
		(*it)->upDate();
	}
}

void RoadMap::printStatus()
{

}