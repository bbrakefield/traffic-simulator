#define _CRT_SECURE_NO_DEPRECATE // Shut up MS, about strcpy(), etc.
#define _CRT_SECURE_NO_WARNINGS  //   just don't say anything.
#include "Intersection.h"
#include <iostream>

Intersection::Intersection()
{
    m_iIDnum = 0;
    m_dCPY = 0.0;
    m_dCPX = 0.0;
    *m_cNorthName = NULL;
    *m_cSouthName = NULL;
    *m_cEastName = NULL;
    *m_cWestName = NULL;
	m_tl = new TrafficLight();
}

Intersection::~Intersection()
{

}

bool Intersection::isPointInIntersection(double x, double y)
{
    return((x >= m_dULX) && (x <= m_dLRX) && (y >= m_dULY) && (y <= m_dLRY));
}

void Intersection::upDate()
{
	// update traffic light
	m_tl->upDate();
}

int Intersection::getIDnum()
{
    return m_iIDnum;
}

void Intersection::setIDnum(int ID)
{
    m_iIDnum = ID;
}

double Intersection::getCPX()
{
    return m_dCPX;
}

void Intersection::setCPX(double cx)
{
    m_dCPX = cx;
}

double Intersection::getCPY()
{
    return m_dCPY;
}

void Intersection::setCPY(double cy)
{
    m_dCPY = cy;
}

char* Intersection::getNorthName()
{
    return m_cNorthName;
}

void Intersection::setNorthName(char* name)
{
    strcpy(m_cNorthName, name);
}

char* Intersection::getSouthName()
{
    return m_cSouthName;
}

void Intersection::setSouthName(char* name)
{
    strcpy(m_cSouthName, name);
}

char* Intersection::getEastName()
{
    return m_cEastName;
}

void Intersection::setEastName(char* name)
{
    strcpy(m_cEastName, name);
}

char* Intersection::getWestName()
{
    return m_cWestName;
}

void Intersection::setWestName(char* name)
{
    strcpy(m_cWestName, name);
}

void Intersection::setULX(double ulx)
{
    m_dULX = ulx;
}

void Intersection::setULY(double uly)
{
    m_dULY = uly;
}
        
void Intersection::setLRX(double lrx)
{
    m_dLRX = lrx;
}
        
void Intersection::setLRY(double lry)
{
    m_dLRY = lry;
}