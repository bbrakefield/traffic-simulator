#ifndef TRAFFICLIGHT_H
#define TRAFFICLIGHT_H

enum LightStatus {RED, AMBER, GREEN};

#include "Intersection.h"

class TrafficLight
{
    private:
        LightStatus m_NSLightColor;
        LightStatus m_EWLightColor;
    public:
        TrafficLight();
        ~TrafficLight();
        void upDate();
        LightStatus getNSStatus();
        void setNSStatus(LightStatus);
        LightStatus getEWStatus();
        void setNSStatus(LightStatus);
};
#endif