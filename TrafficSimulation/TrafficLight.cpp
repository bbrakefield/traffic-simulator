#include "TrafficLight.h"

TrafficLight::TrafficLight()
{
	m_NSLightColor = RED;
	m_EWLightColor = RED;
	m_intersection = NULL;
}

TrafficLight::~TrafficLight()
{
	
}

void TrafficLight::upDate()
{
	// increment a seconds counter
	// if time to change light color do so
}

LightStatus TrafficLight::getNSStatus()
{
	return m_NSLightColor;
}

void TrafficLight::setNSStatus(LightStatus stat)
{
	m_NSLightColor = stat;
}

LightStatus TrafficLight::getEWStatus()
{
	return m_EWLightColor;
}

void TrafficLight::setNSStatus(LightStatus stat)
{
	m_EWLightColor = stat;
}

Intersection* TrafficLight::getIntersection()
{
	return m_intersection;
}

void TrafficLight::setIntersection(Intersection* inter)
{
	m_intersection = inter;
}