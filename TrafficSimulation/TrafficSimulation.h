#ifndef TRAFFICSIMULATION_H
#define TRAFFICSIMULATION_H

#include "Vehicle.h"
#include "RoadMap.h"
#include <vector>
#include <iostream>
#include <sys/types.h>
#include <sys/timeb.h>
#include <time.h>

using namespace std;


class TrafficSimulation 
{
    private:
        vector<Vehicle*> m_vehicles;
        RoadMap *m_rMap;
    public:
        TrafficSimulation();
        ~TrafficSimulation();
        void initSimulation();
        void runSimulation();
        void printReport();
};
#endif