#define _CRT_SECURE_NO_DEPRECATE // Shut up MS, about strcpy(), etc.
#define _CRT_SECURE_NO_WARNINGS  //   just don't say anything.
#include "Road.h"

using namespace std;

//-----------------------------------------
// Default Constructor
//-----------------------------------------
Road::Road()
{
    *m_cRoadName = NULL;
    m_dSPX = 0.0;
    m_dSPY = 0.0;
    m_dEPX = 0.0;
    m_dEPY = 0.0;
    m_iNumLanes = 0;
    m_iStartIntersection = 0;
    m_iEndIntersection = 0;
    m_dSpeedLimit = 0;
    m_dULX = 0.0;
    m_dULY = 0.0;
    m_dLRX = 0.0;
    m_dLRY = 0.0;
}

//-----------------------------------------
// Default Destructor
//-----------------------------------------
Road::~Road()
{
    
}

//-----------------------------------------
// Determine if vehicle point is on road area rectangle. 
//-----------------------------------------
bool Road::isPointOnRoad(double vx, double vy)
{
    return ((vx >= m_dULX) && (vx <= m_dLRX) && (vy >= m_dULY) && (vy <= m_dLRY));
}

char* Road::getRoadName()
{
    return m_cRoadName;
}

void Road::setRoadName(char* name)
{
    strcpy(m_cRoadName, name);
}

double Road::getSPX()
{
    return m_dSPX;
}

void Road::setSPX(double sx)
{
    m_dSPX = sx;
}

double Road::getSPY()
{
    return m_dSPX;
}

void Road::setSPY(double sy)
{
    m_dSPY = sy;
}

double Road::getEPX()
{
    return m_dEPX;
}

void Road::setEPX(double ex)
{
    m_dEPX = ex;
}

double Road::getEPY()
{
    return m_dEPY;
}

void Road::setEPY(double ey)
{
    m_dEPY = ey;
}

int Road::getNumLanes()
{
    return m_iNumLanes;
}

void Road::setNumLanes(int num)
{
    m_iNumLanes = num;
}

int Road::getStartIntersection()
{
    return m_iStartIntersection;
}

void Road::setStartIntersection(int ID)
{
    m_iStartIntersection = ID;
}

int Road::getEndIntersection()
{
    return m_iEndIntersection;
}

void Road::setEndIntersection(int ID)
{
    m_iEndIntersection = ID;
}

double Road::getSpeedLimit()
{
    return m_dSpeedLimit;
}

void Road::setSpeedLimit(double limit)
{
    m_dSpeedLimit = limit;
}

void Road::setULX(double ulx)
{
    m_dULX = ulx;
}

void Road::setULY(double uly)
{
    m_dULY = uly;
}
        
void Road::setLRX(double lrx)
{
    m_dLRX = lrx;
}
        
void Road::setLRY(double lry)
{
    m_dLRY = lry;
}





