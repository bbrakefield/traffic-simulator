#ifndef ROADMAP_H
#define ROADMAP_H

#include <iostream>
#include <vector>
#include "TrafficSimDataParser.h"
#include "Road.h"
#include "Intersection.h"
#include <iostream>

using namespace std;

class RoadMap 
{
    private:
        vector<Road*> m_roads;
        vector<Intersection*> m_intersections;

    public:
        RoadMap();
        ~RoadMap();
        RoadMap(TrafficSimDataParser *dp);
        Road *getRoad(double x, double y, double dir);
        Road *getRoad(char *rdID);
        Intersection *getIntersection(int id);
        Intersection *getNextIntersection(double x, double y, double dir);
        Intersection *getIntersection(double x, double y);
        void update(double time);
        void printStatus();

};
#endif