#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "TrafficLight.h"
#include <iostream>

class Intersection 
{
    private:
        int m_iIDnum;
        double m_dCPX;
        double m_dCPY;
        double m_dULX;
        double m_dULY;
        double m_dLRX;
        double m_dLRY;
        char *m_cNorthName;
        char *m_cSouthName;
        char *m_cEastName;
        char *m_cWestName;
		

    public:
        TrafficLight* m_tl;
        Intersection();
        ~Intersection();
        bool isPointInIntersection(double, double);
        void upDate(); // tell traffic light to update
        int getIDnum();
        void setIDnum(int ID);
        double getCPX();
        void setCPX(double cx);
        double getCPY();
        void setCPY(double cy);
        char *getNorthName();
        void setNorthName(char* name);
        char *getSouthName();
        void setSouthName(char* name);
        char *getEastName();
        void setEastName(char* name);
        char *getWestName();
        void setWestName(char* name);
        void setULX(double);
        void setULY(double);
        void setLRX(double);
        void setLRY(double);
};

#endif
