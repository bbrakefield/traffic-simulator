#include "TrafficSimulation.h"
#include <time.h>
#include <stdlib.h>

void main ()
{
    srand((unsigned int)(time(NULL)));
    TrafficSimulation *ts = new TrafficSimulation();
    ts->runSimulation();
}