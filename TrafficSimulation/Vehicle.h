//===========================================================
// Vehicle.h
// Purpose: Interface file for the Vehicle class.
// Author: Dr. Rick Coleman
//===========================================================
#ifndef VEHICLE_H
#define VEHICLE_H

using namespace std;

class Vehicle
{
	private:
		char *m_cType;
		int m_iIDNumber;
		double m_dStartPointX;
		double m_dStartPointY;
		double m_dCurrPointX;
		double m_dCurrPointY;
		double m_dStartDir;
		double m_dCurrDir;
		double m_dAccel;
		double m_dSpeedMPS;
		double m_dSpeedMPH;

	public:
		Vehicle();
		~Vehicle();
        void move();
        void printReport();
		char *getType();
		void setType(char*);
		int getIDNumber();
		void setIDNumber(int);
		double getSPX();
		void setSPX(double);
		double getSPY();
		void setSPY(double);
		double getCurrPX();
		void setCurrPX(double);
		double getCurrPY();
		void setCurrPY(double);
		double getStartDir();
		void setStartDir(double);
		double getCurrDir();
		void setCurrDir(double);
		double getAccel();
		void setAccel(double);
		double getSpeedMPS();
		void setSpeedMPS(double);
		double getSpeedMPH();
		void setSpeedMPH(double);
};
#endif