#ifndef ROAD_H
#define ROAD_H

#include <iostream>

class Road 
{
    private:
        char *m_cRoadName;
        double m_dSPX;
        double m_dSPY;
        double m_dEPX;
        double m_dEPY;
        int m_iNumLanes;
        int m_iStartIntersection;
        int m_iEndIntersection;
        double m_dSpeedLimit;
        double m_dULX;
        double m_dULY;
        double m_dLRX;
        double m_dLRY;

    public:
        Road();
        ~Road();
        bool isPointOnRoad(double, double);
        char *getRoadName();
        void setRoadName(char*);
        double getSPX();
        void setSPX(double);
        double getSPY();
        void setSPY(double);
        double getEPX();
        void setEPX(double);
        double getEPY();
        void setEPY(double);
        int getNumLanes();
        void setNumLanes(int);
        int getStartIntersection();
        void setStartIntersection(int);
        int getEndIntersection();
        void setEndIntersection(int);
        double getSpeedLimit();
        void setSpeedLimit(double);
        void setULX(double);
        void setULY(double);
        void setLRX(double);
        void setLRY(double);

};
#endif