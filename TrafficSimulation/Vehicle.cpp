#include "Vehicle.h"
#include <string>
#include <iostream>

Vehicle::Vehicle()
{
    *m_cType = NULL;
	m_iIDNumber = 0;
	m_dStartPointX = 0.0;
	m_dStartPointY = 0.0;
	m_dCurrPointX = 0.0;
	m_dCurrPointY = 0.0;
	m_dStartDir = 0.0;
	m_dCurrDir = 0.0;
	m_dAccel = 0.0;
	m_dSpeedMPS = 0.0;
	m_dSpeedMPH = 0.0;
}

Vehicle::~Vehicle()
{
}

void Vehicle::move()
{
    // Get current X, Y location of vehicle
    double curX = this->getCurrPX();
    double curY = this->getCurrPY();
    double distInOneSec = this->getSpeedMPS();
    double newX = 0.0, newY = 0.0;

    // Calculate distance to travel in one second (mps)
    if(this->getCurrDir() == 0.0) // if moving east
    {
        newX = curX + this->getSpeedMPS();
        newY = curY;
    }
    else if (this->getCurrDir() == 90.0) // moving north
    {
        newX = curX;
        newY = curY - this->getSpeedMPS();
    }
    else if(this->getCurrDir() == 180.0) // moving west
    {
        newX = curX - this->getSpeedMPS();
        newY = curY;
    }
    else if(this->getCurrDir() == 270.0)
    {
        newX = curX;
        newY = curY + this->getSpeedMPS();
    }
}

void Vehicle::printReport()
{
	// Print ID, location, road , metters from road origin, and current traveling speed.
	// need to figure out how to print the road the vehicle is on
	cout << "Vehicle " << m_iIDNumber << " is at point (" << m_dCurrPointX << "," << m_dCurrPointY << ") on " << endl;
}

char* Vehicle::getType()
{
    return m_cType;
}

void Vehicle::setType(char *type)
{
    strcpy(m_cType, type);
}

int Vehicle::getIDNumber()
{
    return m_iIDNumber;
}

void Vehicle::setIDNumber(int ID)
{
    m_iIDNumber = ID;
}


double Vehicle::getSPX()
{
    return m_dStartPointX;
}

void Vehicle::setSPX(double sx)
{
    m_dStartPointX = sx;
}

double Vehicle::getSPY()
{
    return m_dStartPointY;
}

void Vehicle::setSPY(double sy)
{
    m_dStartPointY = sy;
}

double Vehicle::getCurrPX()
{
    return m_dCurrPointX;
}
		
void Vehicle::setCurrPX(double cx)
{
    m_dCurrPointX = cx;
}
		
double Vehicle::getCurrPY()
{
    return m_dCurrPointY;
}

void Vehicle::setCurrPY(double cy)
{
    m_dCurrPointY = cy;
}
		
double Vehicle::getStartDir()
{
    return m_dStartDir;
}
		
void Vehicle::setStartDir(double dir)
{
    m_dStartDir = dir;
}
		
double Vehicle::getCurrDir()
{
    return m_dCurrDir;
}

void Vehicle::setCurrDir(double dir)
{
    m_dCurrDir = dir;
}
		
double Vehicle::getAccel()
{
    return m_dAccel;
}
		
void Vehicle::setAccel(double acc)
{
    m_dAccel = acc;
}
		
double Vehicle::getSpeedMPS()
{
    return m_dSpeedMPS;
}
		
void Vehicle::setSpeedMPS(double mps)
{
    m_dSpeedMPS = mps;
}
		
double Vehicle::getSpeedMPH()
{
    return m_dSpeedMPH;
}
		
void Vehicle::setSpeedMPH(double mph)
{
    m_dSpeedMPH = mph;
}
