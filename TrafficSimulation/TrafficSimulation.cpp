#include "TrafficSimulation.h"
#include "TrafficSimDataParser.h"
#include "RoadMap.h"
#include "Vehicle.h"
#include <iostream>

//-----------------------------------------
// Default Constructor
//-----------------------------------------
TrafficSimulation::TrafficSimulation() 
{
    initSimulation();
}

//-----------------------------------------
// Default Destructor
//-----------------------------------------
TrafficSimulation::~TrafficSimulation()
{
}

//-----------------------------------------
// Initialize Simulation
//-----------------------------------------
void TrafficSimulation::initSimulation()
{
    // Prompt user for the name of a file for the parser to parse
    cout << "Please enter the name of the data file and press \"Enter\":" << endl;
    char filename[128];
    cin.getline(filename, 128);

    // Create an instance of the Data Parser
    TrafficSimDataParser *parser = new TrafficSimDataParser(filename);

    // Create an instance of RoadMap and pass it the instance of DataParser
    m_rMap = new RoadMap(parser);

    // Create a collection of vehicles
    // Do I have to if the collection is already defined in the interface? 
    int size = parser->getVehicleCount();
    m_vehicles.reserve(size);

    for(int i = 0; i < size; ++i)
    {
        m_vehicles.push_back(new Vehicle());
    }

    for(vector<Vehicle*>::iterator itr = m_vehicles.begin(); itr != m_vehicles.end(); ++itr)
    {
        // temp variables for copying
        char* temp_type = NULL;
        strcpy(temp_type, (*itr)->getType());
        int temp_id = (*itr)->getIDNumber();
        double temp_xpos = (*itr)->getSPX();
        double temp_ypos = (*itr)->getSPY();
        double temp_dir = (*itr)->getStartDir();
        double temp_acc = (*itr)->getAccel();

        // store data in temp variables
        parser->getVehicleData(temp_type, &temp_id, &temp_xpos, 
                &temp_ypos, &temp_dir, &temp_acc);

        // set fields to value stored in temp variables
        (*itr)->setType(temp_type);
        (*itr)->setIDNumber(temp_id);
        (*itr)->setSPX(temp_xpos);
        (*itr)->setSPY(temp_ypos);
        (*itr)->setStartDir(temp_dir);
        (*itr)->setAccel(temp_acc);
    }
}

//-----------------------------------------
// Run Simulation
//-----------------------------------------
void TrafficSimulation::runSimulation()
{
    int simSpeed;
    int secCount = 0;
    struct _timeb tStruct;
    double thisTime;
    double nextTime = 0.0;
    bool done = false;
    double simTime = 0.0;
    double outputTime = 0.0;
   
    while(!done)
    {
        _ftime_s(&tStruct);
        thisTime = tStruct.time + ((double)(tStruct.millitm) / 1000.0);
        if(thisTime >= nextTime)
        {
            secCount++;
             // Tell the road network to update
             // Update the state of all traffic ights
            m_rMap->update(simTime);

             // Update all the vehicles
             // make all vehicles move
            for(vector<Vehicle*>::iterator it = m_vehicles.begin(); it != m_vehicles.end(); ++it)
            {
				/* 
                    TODO: Update vehicle (note: it should adjust its' speed as needed,
                    determine its' new location allowing for turns, and move itself to
                    the new location.)

                    Setting vehicle speed

                    AB = Distance to intersection
                    SL = Speed limit on this road
                    VS = Vehicle speed
                    TSDVS = Toatl Stopping Distance at VS*
                    TSDSL = TSD at road speed limit*
                    AC' = CX = VX in this case
                    TLS = Traffic light state (RED, AMBER, GREEN);

                    Collect data... NOTE This is for the vehicle moving East.
                    This must be adapted for moving the vehicle North, South, or West.


                */ 

                double VX = (*it)->getCurrPX();
                double VY = (*it)->getCurrPY();
                double CX, CY, BC_prime, AC_prime, AB, SL, VSmps, VSmph, TSDVS, TSDSL, SLmps;
                LightStatus TLS;
                 
                Road* thisRoad = m_rMap->getRoad(VX, VY, (*it)->getCurrDir);
                Intersection* theNextInter = m_rMap->getNextIntersection(VX, VY, (*it)->getCurrDir);

                int NumLanesNS = 0;
                int NumLanesEW = 0;
                if((*it)->getCurrDir() == 0.0) // moving east
                {
                    //Figure out NumLanesNS
                    if(theNextInter->getNorthName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getNorthName());
                        NumLanesNS = tempRoad->getNumLanes();
                    }
                    else if(theNextInter->getSouthName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getSouthName());
                        NumLanesNS = tempRoad->getNumLanes();
                    }
                    else
                    {
                        cout << "Unable to determine number of lanes on NS road" << endl;
                    }

                    CX= theNextInter->getCPX();
                    BC_prime = (NumLanesNS == 4) ? (3.6 * 2.0) : (3.6 * 1.0);
                    AC_prime = CX - VX;
                    AB = AC_prime - BC_prime;
                    TLS = theNextInter->m_tl->getEWStatus();

                }
                else if((*it)->getCurrDir() == 180.0) // for moving west
                {
                     //Figure out NumLanesNS
                    if(theNextInter->getNorthName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getNorthName());
                        NumLanesNS = tempRoad->getNumLanes();
                    }
                    else if(theNextInter->getSouthName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getSouthName());
                        NumLanesNS = tempRoad->getNumLanes();
                    }
                    else
                    {
                        cout << "Unable to determine number of lanes on NS road" << endl;
                    }
                    CX= theNextInter->getCPX();
                    BC_prime = (NumLanesNS == 4) ? (3.6 * 2.0) : (3.6 * 1.0);
                    AC_prime = VX - CX;
                    AB = AC_prime - BC_prime;
                    TLS = theNextInter->m_tl->getEWStatus();
                }
                else if ((*it)->getCurrDir() == 90.0) // if going north
                {
                    if(theNextInter->getEastName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getEastName());
                        NumLanesEW = tempRoad->getNumLanes();
                    }
                    else if(theNextInter->getWestName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getWestName());
                        NumLanesEW = tempRoad->getNumLanes();
                    }
                    else
                    {
                        cout << "Unable to determine number of lanes on NS road" << endl;
                    }  
                    CY = theNextInter->getCPY();
                    BC_prime = (NumLanesEW == 4) ? (3.6 * 2.0) : (3.6 * 1.0);
                    AC_prime = VY - CY;
                    AB = AC_prime - BC_prime;
                    TLS = theNextInter->m_tl->getNSStatus();
                }  
                else if ((*it)->getCurrDir() == 270.0) // if going north
                {
                    if(theNextInter->getEastName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getEastName());
                        NumLanesEW = tempRoad->getNumLanes();
                    }
                    else if(theNextInter->getWestName() != NULL)
                    {
                        Road * tempRoad = m_rMap->getRoad(theNextInter->getWestName());
                        NumLanesEW = tempRoad->getNumLanes();
                    }
                    else
                    {
                        cout << "Unable to determine number of lanes on NS road" << endl;
                    }  
                    CY = theNextInter->getCPY();
                    BC_prime = (NumLanesEW == 4) ? (3.6 * 2.0) : (3.6 * 1.0);
                    AC_prime = CY - VY;
                    AB = AC_prime - BC_prime;
                    TLS = theNextInter->m_tl->getNSStatus();
                }  
                
                SL = thisRoad->getSpeedLimit();
                VSmps = (*it)->getSpeedMPS();
                VSmph = (*it)->getSpeedMPH();
                TSDVS = 1.1 * VSmph + 0.06 * (pow(VSmph, 2));
                TSDVS *= 0.3048; // converts to meters to stop
                TSDSL = 1.1 * thisRoad->getSpeedLimit() + 0.06 * (pow(thisRoad->getSpeedLimit(), 2));
                TSDSL *= 0.3048;
                SLmps = SL * 0.44704;

                // Handle light being RED or AMBER
                if((TLS == RED) || (TLS == AMBER))
                {
                    if(TSDSL < AB)
                    {
                         // OK to accelerate
                        if(VSmps < SLmps)  // if not at speed limit in m/s
                        {
                            VSmps+=(*it)->getAccel();  // Inscrease speed by accel.
                            VSmph = VSmps / 0.44704;   // set speed in MPH
                            if(VSmps > SLmps)   //Check if over the speed limit
                            {
                                VSmps = SLmps;  // if over set speed to SL
                                VSmph = VSmps / 0.44704;    // also set MPH
                            }
                        }
                    }
                    // If dist to stop > dis to intersection then slow down
                    else if((TSDVS > AB) && (VSmps != 0))
                    {
                        while ((TSDVS > AB) && (AB > 3.6) && (VSmps > 0))
                        {
                            VSmps -= (*it)->getAccel();
                            VSmph = VSmps / 0.44704;
                            if(VSmps < 0)
                            {
                                 VSmps = 0;
                                 VSmph = 0;
                            }
                            TSDVS = 1.1 * VSmph + 0.06 * (pow(VSmph, 2));
                            TSDVS *= 0.3048; // converts to meters to stop
                            if(VSmps == 0)
                            {
                                break;
                            }
                        }
                    }

                    if(AB < 3.6)
                    {
                        VSmps = 0;
                        VSmph = 0;
                    }
                }
                else // handle light being green
                {
                    if(VSmps < SLmps)
                    {
                        VSmps += (*it)->getAccel();
                        VSmph = VSmps / 0.44704;
                        if(VSmps > SLmps)
                        {
                            VSmps = SLmps;
                            VSmph = VSmps / 0.44704;
                        }
                    }
                }
                // Set new speed mph and mps in vehicle object;
                (*it)->setSpeedMPH(VSmph);
                (*it)->setSpeedMPS(VSmps);

			    //(*it)->move();
            }
        }

        // Check for 5 second interval to print status to screem
        if(thisTime >= outputTime)
        {
            cout << "Traffic Sim Elapsed Time: " << secCount << endl;

            // Call appropriate objects to output their status
            m_rMap->printStatus();
            for(vector<Vehicle*>::iterator it = m_vehicles.begin(); it != m_vehicles.end(); ++it)
            {
                (*it)->printReport();
            }
            outputTime += 5.0;
        }
    }
}

//-----------------------------------------
// Print Simulation Report
//-----------------------------------------
void TrafficSimulation::printReport()
{
    // Get and report status of traffic light
    // Call roadmap printstatus
    m_rMap->printStatus();

    // Call each vehicle.printStatus
    for(vector<Vehicle*>::iterator itr = m_vehicles.begin(); itr != m_vehicles.end(); ++itr)
    {
        (*itr)->printReport();
    }
}